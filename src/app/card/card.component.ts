import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModelComponent } from '../model/model.component';
import { CardService } from '../card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  public cards;
  public editoption;
  public editvalue = '';
  public showbtn = true;
  public inputdata = '';
  public showinput = false;
  constructor(
    public dialog: MatDialog,
    private _cardservies: CardService,
    public model: MatDialog
  ) {}
  @Input() data;
  @Input() id;

  ngOnInit(): void {
    this.cards = this.data.filter((data) => {
      if (data.id == this.id) {
        return data;
      }
    })[0].cards;

    if (this.cards == undefined) {
      this.cards = [];
    }
  }

  onselect($event, id) {
    $event.stopPropagation();

    this.editoption = id;
  }
  hideinput() {
    this.editoption = '';
  }

  addcard($event, id) {
    if (this.inputdata == '') {
      return true;
    }

    this._cardservies.addcard(this.inputdata, id).subscribe((data) => {
      this.cards.push(data);
    });
    this.showaddinput();
  }

  editcard($event, id) {
    if (this.editvalue == '') {
      return true;
    }

    this._cardservies.editcard(id, this.editvalue).subscribe((data) => {
      return this.cards.map((item) => {
        if (item.id == id) {
          item.name = this.editvalue;
        }
      });
    });
    this.hideinput();
  }
  deletecard($event, id) {
    this._cardservies.deletecard(id).subscribe();

    this.cards = this.cards.filter((data) => {
      return data.id != id;
    });
    this.hideinput();
  }
  showaddinput() {
    this.showinput = !this.showinput;
    this.showbtn = !this.showbtn;
  }

  openmodel($event, id) {
    $event.stopPropagation();
    let carddata = this.cards.filter((item) => {
      if (item.id == id) {
        return item;
      }
    });

    let dialogRef = this.model.open(ModelComponent, {
      width: '768px',
      height: '800px',
      data: { data: carddata, id: id, name: carddata[0].name },
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }
}

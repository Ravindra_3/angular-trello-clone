import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {BoardDetailsComponent} from './board-details/board-details.component'
import {BoardComponent} from './board/board.component'
import { HomeComponent } from './home/home.component';




const routes: Routes = [
   { path: '', component: HomeComponent },
   { path: 'board/:id', component: BoardDetailsComponent }
  
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule] 
})
export class AppRoutingModule {}

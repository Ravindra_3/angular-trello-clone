import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-addboard-input',
  templateUrl: './addboard-input.component.html',
  styleUrls: ['./addboard-input.component.css'],
})
export class AddboardInputComponent implements OnInit {
  public fromDialog = '';
  public id;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<any>
  ) {}
  ngOnInit(): void {}
  save = (id) => {
    if (this.fromDialog == '') {
      return true;
    }
    this.dialogRef.close({ event: 'close', data: this.fromDialog, id: id });
  };
  close = () => {
    this.dialogRef.close();
  };
}

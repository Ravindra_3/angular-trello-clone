import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddboardInputComponent } from './addboard-input.component';

describe('AddboardInputComponent', () => {
  let component: AddboardInputComponent;
  let fixture: ComponentFixture<AddboardInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddboardInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddboardInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

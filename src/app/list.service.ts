import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class ListService {
  constructor(private http: HttpClient) {}

  getAllListData(id): Observable<any> {
    let url = `https://api.trello.com/1/boards/${id}/lists?cards=all&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;
    return this.http
      .get<any>(url)
      .pipe
      // catchError(e=>console.log(err))
      ();
  }

  addlist(data, id): Observable<any> {
    let name = data;
    let url = `https://api.trello.com/1/lists?name=${name}&idBoard=${id}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http
      .post<any>(url, data)
      .pipe
      // catchError(e=>console.log(err))
      ();
  }

  editlist(id, name) {
    let url = `https://api.trello.com/1/lists/${id}?name=${name}&&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.put<any>(url, null).pipe();
  }

  deletelist(id) {
    let url = `https://api.trello.com/1/lists/${id}/?closed=true&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.put<any>(url, null).pipe();
  }
}

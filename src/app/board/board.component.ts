import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BoardDataService } from '../board-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
})
export class BoardComponent implements OnInit {
  public boards;
  public activeedit = false;
  @Output() newItemEvent = new EventEmitter<string>();

  editboardfn = ($event, id) => {
    $event.stopPropagation();
    $event.preventDefault();

    this.newItemEvent.emit(id);
  };

  constructor(
    private _boardservies: BoardDataService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._boardservies.getBoarddata().subscribe((data) => {
      this.boards = data;
      // console.log(this.boards);
    });
  }
  addboard = (name, id) => {
    this._boardservies.addboard(name).subscribe((data) => {
      this.boards.push(data);
    });
  };
  deleteboard = (data, id) => {
    this._boardservies.deleteboard(data, id).subscribe();
    this.boards = this.boards.filter((data) => {
      return data.id != id;
    });
  };

  editboard = (id, name) => {
    if (name == '') {
      return true;
    }

    this._boardservies.editboard(id, name).subscribe((data) => {
      console.log(data);

      return this.boards.map((item) => {
        if (item.id == id) {
          item.name = name;
        }
      });
    });
  };

  OnSelect($event, val) {
    $event.preventDefault();

    this.router.navigate(['board', val]);
  }
}

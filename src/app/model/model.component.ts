import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CardService } from '../card.service';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css'],
})
export class ModelComponent implements OnInit {
  public checklist;
  public addlist = false;
  public listname = '';
  public editlistdata = '';
  public showeditinput = '';
  public edititemvalue = '';
  public edititemoption = '';
  public additemid = '';
  public additemvalue = '';
  public binding = '';
  public selectedOptions;
  public cardid;
  public showedititem;
  public edititemdata;

  public modeldata = 'hello from model';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<any>,
    private _cardservies: CardService
  ) {}

  ngOnInit(): void {
    this._cardservies.getcarddata(this.data.id).subscribe((data) => {
      this.checklist = data;
      this.cardid = data[0].idCard;
    });
  }
  save = () => {
    this.dialogRef.close({ event: 'close', data: this.modeldata, id: 'hello' });
  };
  close = () => {
    this.dialogRef.close({ event: 'close', data: this.modeldata, id: 'hello' });
  };

  showinput() {
    this.addlist = !this.addlist;
  }

  submitlistname() {
    if (this.listname == '') {
      return true;
    }

    this._cardservies
      .addchecklist(this.data.id, this.listname)
      .subscribe((data) => {
        this.checklist.push(data);
      });

    this.showinput();
  }

  deletechecklist(id) {
    this._cardservies.deletechecklist(id).subscribe();

    this.checklist = this.checklist.filter((data) => {
      return data.id != id;
    });
  }

  showupdateinput(id) {
    this.showeditinput = id;
  }
  hideeditlistinput() {
    this.showeditinput = ''; 
  }

  submitlist(id) {
    if (this.editlistdata == '') {
      return true;
    }
    this._cardservies.editchecklist(id, this.editlistdata).subscribe((data) => {
      return this.checklist.map((item) => {
        if (item.id == id) {
          item.name = this.editlistdata;
        }
      });
    });
    this.hideeditlistinput();
  }

  edititem($event, listid, id) {
    this.edititemoption = id;

    $event.stopPropagation();
  } 
  hideedititem($event) {
    $event.stopPropagation();
    this.edititemoption = '';
  }

  submititem($event, listid) {
    $event.stopPropagation();

    this._cardservies
      .addcheckitem(listid, this.edititemvalue)
      .subscribe((data) => {});

    this.edititemoption = '';
  }

  deleteitem($event, listid, id) {
    $event.stopPropagation();
    this.edititemoption = '';
    this._cardservies
      .deletecheckitem(listid, id, this.edititemvalue)
      .subscribe();

    return this.checklist.map((data) => {
      if (data.id == listid) {
        let a = data.checkItems.filter((data) => {
          return data.id != id;
        });
        data.checkItems = a;
      }
    });
  }
  stopprop($event) {
    $event.stopPropagation();
    return true;
  }

  additemoption($event, listid) {
    this.additemid = listid;
  }
  hideadditem() {
    this.additemid = '';
  }

  submitadditem($event, listid) {
    this._cardservies
      .addcheckitem(listid, this.additemvalue)
      .subscribe((data) => {
        let newitem = data;

        this.checklist.map((data) => {
          if (data.id == listid) {
            let t = data.checkItems.push(newitem);
          }
        });
      });

    this.hideadditem();
  }

  showinputforeditietm($event, index) {
    this.showedititem = index;
  }
  hideinputforeditietm() {
    this.showedititem = '';
  }

  submitedititemnew($event, item, index) {
    $event.stopPropagation();

    this._cardservies
      .editcheckitem(index, this.cardid, this.edititemdata)
      .subscribe((data) => {});
    this.showedititem = '';

    return this.checklist.map((data) => {
      if (data.id == item.id) {
        data.checkItems.map((e) => {
          if (e.id == index) {
            e.name = this.edititemdata;
          }
        });
      }
    });
  }
}

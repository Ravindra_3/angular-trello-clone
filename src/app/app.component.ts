import { BoardDataService } from './board-data.service';
import { BoardComponent } from './board/board.component';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Component, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddboardInputComponent } from './addboard-input/addboard-input.component';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements AfterViewInit {
  public title = 'trello-Angular';
  animal: string = 'dog';
  name: string;

  constructor(public dialog: MatDialog, router: Router) {
    var Navigationend = router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    );
    Navigationend.subscribe((event:NavigationEnd)=>{

  gtag('config', 'UA-171019005-1',{
    'page-path':event.urlAfterRedirects
  })

    })
  }

  @ViewChild(BoardComponent)
  private Board: BoardComponent;

  ngAfterViewInit() {}

  // router.checkEvents() {
  //   this.router.events.subscribe(event => {
  //     switch (true) {
  //       case event instanceof eventName:

  //         break;

  //       default:
  //         break;
  //     }
  //   });
  // }
  // router.events

  opendailog(id?: string) {
    let dialogRef = this.dialog.open(AddboardInputComponent, {
      width: '30em',
      height: '500px',
      data: { name: this.name, id: id, msg: 'New' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result.data !== 'undefined' && result.id == undefined) {
        let boardName = result.data;
        this.Board.addboard(boardName, null);
      }
      if (result.id !== undefined) {
        let boardName = result.data;
        this.Board.editboard(id, boardName);
      }
    });
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { BoardDataService } from './board-data.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { AddboardInputComponent } from './addboard-input/addboard-input.component';
import { BoardDetailsComponent } from './board-details/board-details.component';
import { HomeComponent } from './home/home.component';
import { ListService } from './list.service';
import { CardComponent } from './card/card.component';
import { ModelComponent } from './model/model.component';
import {CardService} from './card.service'
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    AddboardInputComponent,
    BoardDetailsComponent,
    HomeComponent,
    CardComponent,
    ModelComponent,
  ],
  entryComponents: [AddboardInputComponent,ModelComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [BoardDataService, ListService,CardService],
  bootstrap: [AppComponent],
})
export class AppModule {}

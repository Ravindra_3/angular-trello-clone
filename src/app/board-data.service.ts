import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Board } from './board';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class BoardDataService {
  private _url =
    'https://api.trello.com/1/members/me/boards?fields=name,url&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5';

  constructor(private http: HttpClient) {}

  getBoarddata(): Observable<any> {
    return this.http
      .get<Board[]>(this._url)
      .pipe
      // catchError(e=>console.log(err))
      ();
  }

  addboard(data): Observable<any> {
    let name = data;
    let url = `https://api.trello.com/1/boards/?name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http
      .post<any>(url, data)
      .pipe
      // catchError(e=>console.log(err))
      ();
  }

  deleteboard(data, id): Observable<any> {
    let del = id;
    let url = `https://api.trello.com/1/boards/${del}?key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.delete<any>(url).pipe();
  }

  editboard(id, name) {
    let url = `https://api.trello.com/1/boards/${id}?name=${name}&&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.put<any>(url, null).pipe();
  }
}

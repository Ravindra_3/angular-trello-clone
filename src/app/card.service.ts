import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CardService {
  constructor(private http: HttpClient) {}

  addcard(name, id): Observable<any> {
    let url = `https://api.trello.com/1/cards?idList=${id}&name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.post<any>(url, null).pipe();
  }

  editcard(id, name) {
    let url = `https://api.trello.com/1/cards/${id}?name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.put<any>(url, null).pipe();
  }

  deletecard(id) {
    let url = `https://api.trello.com/1/cards/${id}?key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.delete<any>(url).pipe();
  }

  getcarddata(id): Observable<any> {
    let url = `https://api.trello.com/1/cards/${id}/checklists?key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;
    return this.http
      .get<any>(url)
      .pipe
      // catchError(e=>console.log(err))
      ();
  }
  addchecklist(id, name): Observable<any> {
    let url = `https://api.trello.com/1/checklists?idCard=${id}&name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http
      .post<any>(url, null)
      .pipe
      // catchError(e=>console.log(err))
      ();
  }

  deletechecklist(id): Observable<any> {
    let url = `https://api.trello.com/1/checklists/${id}?key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.delete<any>(url).pipe();
  }

  editchecklist(id, name) {
    let url = `https://api.trello.com/1/checklists/${id}?name=${name}&&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.put<any>(url, null).pipe();
  }

  deletecheckitem(listid, id, name) {
    let url = `https://api.trello.com/1/checklists/${listid}/checkItems/${id}?key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;
    return this.http.delete<any>(url).pipe();
  }

  addcheckitem(listid, name): Observable<any> {
    let url = `https://api.trello.com/1/checklists/${listid}/checkItems?name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;

    return this.http.post<any>(url, null).pipe();
  }
  editcheckitem(itemid, cardid, name) {
    let url = `https://api.trello.com/1/cards/${cardid}/checkItem/${itemid}?name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`;
    return this.http.put<any>(url, null).pipe();
  }
}

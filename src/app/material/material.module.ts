import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input'
import {ReactiveFormsModule,FormsModule} from '@angular/forms';
import {MatListModule} from '@angular/material/list';
import {MatProgressBarModule} from '@angular/material/progress-bar';

const MaterialComponents = [
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatToolbarModule,
  MatIconModule,
  MatDialogModule,
  MatInputModule,
  ReactiveFormsModule,
  FormsModule,
  MatListModule,
  MatProgressBarModule
];

@NgModule({
  imports: [MaterialComponents],
  exports: [MaterialComponents],
})
export class MaterialModule {}

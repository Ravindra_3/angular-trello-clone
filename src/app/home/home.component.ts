import { BoardDataService } from '../board-data.service';
import { BoardComponent } from '../board/board.component';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Component, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddboardInputComponent } from '../addboard-input/addboard-input.component';

@Component({
  selector: 'app-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements AfterViewInit {
  public title = 'trello-Angular';
  animal: string='dog'
  name: string;
  constructor(public dialog: MatDialog) {}

  @ViewChild(BoardComponent)
  private Board: BoardComponent;

  ngAfterViewInit() {}

  opendailog(id?:string) {
  
    let dialogRef = this.dialog.open(AddboardInputComponent, {
      width: '30em',
      height: '500px',
      data: { name: this.name, id:id ,msg:'New'},
    });

    dialogRef.afterClosed().subscribe((result) => {
 
      if(result.data!=="undefined" && result.id ==undefined){
        let boardName = result.data;
        this.Board.addboard(boardName, null);
      }
      if(result.id !==undefined){
        let boardName = result.data;
      this.Board.editboard(id,boardName)
}

    });
  }
}


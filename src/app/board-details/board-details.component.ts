import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListService } from '../list.service';

@Component({
  selector: 'app-board-details',
  templateUrl: './board-details.component.html',
  styleUrls: ['./board-details.component.css'],
})
export class BoardDetailsComponent implements OnInit {
  public boardid;
  public name: Object;
  public childAllLists;
  public showbtn = true;
  public showinput = false;
  public inputdata = '';
  public showeditinput;
  public showlistname;
  public editdata = '';

  constructor(
    private route: ActivatedRoute,
    private _listservies: ListService
  ) {}

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.boardid = id;

    this.name = this._listservies.getAllListData(id).subscribe((data) => {
      this.childAllLists = data;
    });
  }
  // for card model

  onselect($event, id) {
    $event.stopPropagation();
    $event.preventDefault();
  }

  // for adding list

  showaddinput = () => {
    this.showbtn = !this.showbtn;
    this.showinput = !this.showinput;
  };

  addlist(event) {
    let name = this.inputdata;
    if (name == '') {
      return true;
    }
    this._listservies.addlist(name, this.boardid).subscribe((data) => {
      this.childAllLists.push(data);
    });
    this.showbtn = !this.showbtn;
    this.showinput = !this.showinput;
  }

  editlist(val) {
    this.showeditinput = val;
    this.showlistname = val;
  }

  submitedit(val) {
    if (this.editdata == '') {
      return true;
    }

    this._listservies.editlist(val, this.editdata).subscribe((data) => {
      return this.childAllLists.map((item) => {
        if (item.id == val) {
          item.name = this.editdata;
        }
      });
    });
    // console.log(this.childAllLists);
    this.hideinput();
  }

  hideinput() {
    this.showeditinput = '';
    this.showlistname = '';
  }

  deletelist($event, id) {
    $event.stopPropagation();
    this._listservies.deletelist(id).subscribe();
    // console.log('deleted')
    this.childAllLists = this.childAllLists.filter((data) => {
      return data.id != id;
    });
  }
}
